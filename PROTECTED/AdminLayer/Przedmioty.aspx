﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/AdminLayer/MasterAdmin.master" AutoEventWireup="true" CodeFile="Przedmioty.aspx.cs" Inherits="PROTECTED_AdminLayer_Przedmioty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div><h1>Przedmioty</h1></div>
    <div>
        <asp:TextBox ID="TextBox_Szukaj_Przedmiot" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Szukaj" OnClick="Button1_Click" />
         <asp:Button ID="Button2" runat="server" Text="Dodaj" OnClick="Button2_Click" /> 
    </div>
    <div>
        <asp:GridView ID="GridView_Przedmioty" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                <asp:BoundField DataField="nazwa" HeaderText="Nazwa Przedmiotu" />
                <asp:BoundField DataField="wyk_l_godzin" HeaderText="Liczba godzin wykładu" />
                <asp:BoundField DataField="lab_l_godzin" HeaderText="Liczba godzin laboratorium" />
                <asp:CheckBoxField   DataField="egzamin" HeaderText="Egzamin" />
                <asp:BoundField DataField="rodzaj" HeaderText="Rodzaj Przedmiotu" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Columns>
        </asp:GridView>
        <asp:Label ID="Label_brak_rekordu" runat="server"></asp:Label>
    </div>
</asp:Content>

