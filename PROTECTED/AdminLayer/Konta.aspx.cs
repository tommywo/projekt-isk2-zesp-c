﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_AdminLayer_Konta : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            daneStudenta();
            daneWykladowcy();
        }
    }
    protected void daneStudenta()
    {

        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
            String.Format("select id, nr_indeksu, imie, nazwisko, adres, data_urodzenia, pesel, data_rozpoczecia, aktualny_sem, tryb_studiow, idkierunku, mail, haslo from Studenci");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        GridView1.Visible = true;
        connection.Close();

    }
    protected void daneWykladowcy()
    {

        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
            String.Format("select id, imie, nazwisko, katedra, email, login, haslo from Wykladowcy");
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView2.DataSource = dt;
        GridView2.DataBind();
        GridView2.Visible = true;
        connection.Close();

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //kasowanie po id, musialem dodać do Gridview DataKeyNames="Id"
        String id = GridView1.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("delete from Studenci where id ={0}", id);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        daneStudenta();
    }

    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //kasowanie po id, musialem dodać do Gridview DataKeyNames="Id"
        String id = GridView2.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText = String.Format("delete from Wykladowcy where id ={0}", id);
        command.CommandType = CommandType.Text;
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        daneWykladowcy();

    }

    //jak anulujesz edytowanie to wraca do domyślnej wartości indeksu dla Gridview
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        daneStudenta();
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        daneWykladowcy();
    }

    //to jest potrzebne do edycji
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        daneStudenta();
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        daneWykladowcy();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView1.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView1.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView1.Rows[e.RowIndex].Cells[i];
            GridView1.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }
        //jeden sposób na edycję
        command.CommandText = string.Format("update Studenci set nr_indeksu='{0}', imie='{1}', nazwisko='{2}', adres='{3}', data_urodzenia='{4}', pesel='{5}', data_rozpoczecia='{6}', aktualny_sem='{7}', tryb_studiow='{8}', kierunek='{9}', mail='{10}', haslo='{11}' where Id='{12}'", e.NewValues["nr_indeksu"], e.NewValues["imie"], e.NewValues["nazwisko"], e.NewValues["adres"], e.NewValues["data_urodzenia"], e.NewValues["pesel"], e.NewValues["data_rozpoczecia"], e.NewValues["aktualny_sem"], e.NewValues["tryb_studiow"], e.NewValues["kierunek"], e.NewValues["mail"], e.NewValues["haslo"], id);
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        GridView1.EditIndex = -1;
        daneStudenta();
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView2.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView2.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView2.Rows[e.RowIndex].Cells[i];
            GridView2.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }

        //drugi sposób na edycję
        command.CommandText = string.Format("update Wykladowcy set imie=@imie, nazwisko=@nazwisko, katedra=@katedra, email=@email, haslo=@haslo, login=@login where Id=@Id");
        command.CommandType = CommandType.Text;
        command.Parameters.AddWithValue("@imie", e.NewValues["imie"]);
        command.Parameters.AddWithValue("@nazwisko", e.NewValues["nazwisko"]);
        command.Parameters.AddWithValue("@katedra", e.NewValues["katedra"]);
        command.Parameters.AddWithValue("@email", e.NewValues["email"]);
        command.Parameters.AddWithValue("@haslo", e.NewValues["haslo"]);
        command.Parameters.AddWithValue("@login", e.NewValues["login"]);
        command.Parameters.AddWithValue("@Id", id);
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();
        GridView2.EditIndex = -1;
        daneWykladowcy();
    }
}
